let featureList;

const environs = {
  place_id: 157901798,
  licence: 'Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright',
  osm_type: 'relation',
  osm_id: 175342,
  boundingbox: [
    51.2867602,
    51.6918741,
    -0.510375,
    0.3340155
  ],
  lat: 51.4893335,
  lon: -0.144055043949045,
  display_name: 'Greater London, England, UK',
  class: 'boundary',
  type: 'administrative',
  importance: 0.71447096677094,
  icon: 'https://nominatim.openstreetmap.org/images/mapicons/poi_boundary_administrative.p.20.png'
};
/*
const environs = { // fake!
  place_id: 159227673,
  licence: 'Data © OpenStreetMap contributors, ODbL 1.0. http://www.openstreetmap.org/copyright',
  osm_type: 'relation',
  osm_id: 2552485,
  boundingbox: [
    40.679542,
    40.882463,
    -74.0477158,
    -73.9071359
  ],
  lat: 40.7835127,
  lon: -73.9570850698202,
  display_name: 'New York County, NYC, New York, United States of America',
  class: 'boundary',
  type: 'administrative',
  importance: 0.69014193609354,
  icon: 'http://nominatim.openstreetmap.org/images/mapicons/poi_boundary_administrative.p.20.png'
};
*/

const tileLayer = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
  //attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
  //  '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
  //  'Imagery © <a href="http://mapbox.com">Mapbox</a>',
  accessToken: 'pk.eyJ1Ijoibm9zYm9ybiIsImEiOiJjaXphdTliOHowMDV0MndwZTAzZ2owM2JnIn0.wlznYbYLvJb9TdJYuLQoCw',
  detectRetina: true,
  id: 'mapbox.streets', // 'mapbox.streets-satellite'
  maxZoom: 19
});

const markersLayer = new L.MarkerClusterGroup({
  //spiderfyOnMaxZoom: true,
  //showCoverageOnHover: false,
  //zoomToBoundsOnClick: true,
  //disableClusteringAtZoom: 16
});

const map = L.map('map', {
  attributionControl: false,
  center: [environs.lat, environs.lon],
  layers: [tileLayer, markersLayer],
  maxBounds: [
    [environs.boundingbox[0], environs.boundingbox[2]],
    [environs.boundingbox[1], environs.boundingbox[3]]
  ],
  maxBoundsViscosity: 1.0,
  zoomControl: false
});

map.on('load', function () {
  console.log('*** map:load ***');
  $.getJSON('features.geojson', function (data) {
    $.each(data.features, function (index, feature) {
      feature._oh = new opening_hours(feature.properties.hours, environs);
      //feature._timer_id = setTimeout(function, 1000);
    });
    openLayer.addData(data);
    //map.addLayer(openLayer);
    markersLayer.addLayer(openLayer);
    //closedLayer.addData(data);
    //map.addLayer(closedLayer);
    //unknownLayer.addData(data);
    //map.addLayer(unknownLayer);
  });
});

map.setMinZoom(
  map.getBoundsZoom([
    [environs.boundingbox[0], environs.boundingbox[2]],
    [environs.boundingbox[1], environs.boundingbox[3]]
  ])
);

L.control.zoom({
  position: 'bottomright',
  zoomInTitle: 'Zoom In',
  zoomOutTitle: 'Zoom Out'
}).addTo(map);

L.control.locate({
  //circleStyle: {
  //  clickable: false,
  //  weight: 1
  //},
  icon: 'fa fa-location-arrow',
  locateOptions: {
    enableHighAccuracy: true,
    maxZoom: 19,
    maximumAge: 10000,
    timeout: 10000,
    watch: true
  },
  markerStyle: {
    fillOpacity: 0.8,
    opacity: 0.8,
    weight: 1
  },
  onLocationError: function(control) {
    console.log('*** locate:onLocationError ***');
    control.stop();
    map.setView([environs.lat, environs.lon], map.getMinZoom());
  },
  onLocationOutsideMapBounds: function(control) {
    console.log('*** locate:onLocationOutsideMapBounds ***');
    control.stop();
    map.setView([environs.lat, environs.lon], map.getMinZoom());
  },
  position: 'bottomright',
  strings: {
    title: 'Show My Location'
  },
}).addTo(map).start();

const openLayer = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    const options = {
      icon: feature.properties.icon || 'circle-o',
      iconAnchor: [16, 16],
      iconSize: [32, 32],
      innerIconAnchor: [0, 8],
    };
    return L.marker(latlng, {
      icon: L.BeautifyIcon.icon(options),
      riseOnHover: true,
      title: feature.properties.name
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      const content = featurePopupContent(feature.properties);
      layer.on({
        click: function () {
          $('#feature-title').html(feature.properties.name);
          $('#feature-info').html(content);
          $('#featureModal').modal('show');
          //highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $('#feature-list tbody').append(
        '<tr class="feature-row" data-id="' + L.stamp(layer) + '" data-lat="' + layer.getLatLng().lat + '" data-lng="' + layer.getLatLng().lng + '">' +
        //'<td style="vertical-align: middle;"><i class="fa fa-check"></i></td>' +
        '<td class="feature-name">' +
        '<div>' + layer.feature.properties.name + '</div>' +
        '<div>' +
        '<small>' + Math.round(map.getCenter().distanceTo(layer.getLatLng())) + 'm&nbsp;&#8226;&nbsp;' +
        (layer.feature._oh.getState() ? 'Open' : 'Closed') + '</small>' +
        '</div>' +
        '</td>' +
        '<td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td>' +
        '</tr>'
      );
    }
  /*
  },
  filter: function (geoJsonFeature) {
    return geoJsonFeature._oh.getStateString() == 'open';
  */
  }
});

function featurePopupContent(properties) {
  let content = '<table class="table table-striped table-bordered table-condensed">';
  //content += '<tr><th>Name</th><td>' + properties.name + '</td></tr>';
  content += '<tr><td><i class="fa fa-map-marker"></i></td><td>' + properties.address.join('<br/>') + '</td></tr>';
  if (properties.phone) {
    content += '<tr><td><i class="fa fa-phone"></i></td><td>' + properties.phone + '</td></tr>';
  }
  if (properties.url) {
    content += '<tr><td><i class="fa fa-external-link"></i></td><td><a class="url-break" href="' + properties.url + '" target="_blank">' + properties.url + '</a></td></tr>';
  }
  content += '<tr><td><i class="fa fa-clock-o"></i></td><td>';
  if (properties.when) {
    content += properties.when;
  } else if (properties.hours) {
    content += properties.hours;
  } else {
    content += 'Unkown';
  }
  content += '</td></tr>';
  let social = '';
  if (properties.facebook) {
    social += '<a href="https://facebook.com/' + properties.facebook + '" target="_blank">';
    social += '<i class="fa fa-lg fa-facebook"></i>';
    social += '</a>';
  }
  if (properties.instagram) {
    social += '<a href="https://www.instagram.com/' + properties.instagram + '/" target="_blank">';
    social += '<i class="fa fa-lg fa-instagram"></i>';
    social += '</a>';
  }
  if (properties.pinterest) {
    social += '<a href="https://pinterest.com/' + properties.pinterest + '/" target="_blank">';
    social += '<i class="fa fa-lg fa-pinterest"></i>';
    social += '</a>';
  }
  if (properties.twitter) {
    social += '<a href="https://twitter.com/' + properties.twitter + '" target="_blank">';
    social += '<i class="fa fa-lg fa-twitter"></i>';
    social += '</a>';
  }
  if (social !== '') {
    content += '<tr><td></td><td><div class="social"><ul>';
    content += social;
    content += '</ul></div></td></tr>';
  }
  content += '<table>';
  return content;
}

/*
var openLayer = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.AwesomeMarkers.icon({
        icon: 'check-circle',
        prefix: 'fa',
        markerColor: 'blue'
      }),
      title: feature.properties.name,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = featurePopupContent(feature.properties);
      layer.on({
        click: function (e) {
          $('#feature-title').html(feature.properties.name);
          $('#feature-info').html(content);
          $('#featureModal').modal('show');
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $('#feature-list tbody').append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/theater.png"></td><td class="feature-name">' + layer.feature.properties.name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
    }
  },
  filter: function (geoJsonFeature) {
    return geoJsonFeature._oh.getStateString() == 'open';
  }
});
*/

/*
var closedLayer = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.AwesomeMarkers.icon({
        icon: 'times-circle',
        prefix: 'fa',
        markerColor: 'blue'
      }),
      title: feature.properties.name,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = featurePopupContent(feature.properties);
      layer.on({
        click: function (e) {
          $('#feature-title').html(feature.properties.name);
          $('#feature-info').html(content);
          $('#featureModal').modal('show');
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $('#feature-list tbody').append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/theater.png"></td><td class="feature-name">' + layer.feature.properties.name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
    }
  },
  filter: function (geoJsonFeature) {
    return geoJsonFeature._oh.getStateString() == 'close';
  }
});
*/

/*
var unknownLayer = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      icon: L.AwesomeMarkers.icon({
        icon: 'question-circle',
        prefix: 'fa',
        markerColor: 'blue'
      }),
      title: feature.properties.name,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      var content = featurePopupContent(feature.properties);
      layer.on({
        click: function (e) {
          $('#feature-title').html(feature.properties.name);
          $('#feature-info').html(content);
          $('#featureModal').modal('show');
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $('#feature-list tbody').append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"><img width="16" height="18" src="assets/img/theater.png"></td><td class="feature-name">' + layer.feature.properties.name + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
    }
  },
  filter: function (geoJsonFeature) {
    return geoJsonFeature._oh.getStateString() == 'unknown';
  }
});
*/

function sortFeatureList() {
  featureList.sort('feature-name', {
    order:'asc',
    sortFunction: function (a, b) {
      const da = map.getCenter().distanceTo([a.values().lat, a.values().lng]);
      const db = map.getCenter().distanceTo([b.values().lat, b.values().lng]);
      return (da - db);
    }
  });
}

L.control.scale({
  position: 'bottomleft'
}).addTo(map);

L.Control.geocoder({
  geocoder: new L.Control.Geocoder.Nominatim({
    geocodingQueryParams: {
      bounded: 1,
      viewbox: [
        environs.boundingbox[2],
        environs.boundingbox[1],
        environs.boundingbox[3],
        environs.boundingbox[0]
      ]
    }
  }),
  position: 'topright',
  showResultIcons: true
}).addTo(map);

$(document).one('ajaxStop', function () {
  $('#loading').hide();

  featureList = new List('features', {
    valueNames: [
      'feature-name',
      { data: ['id', 'lat', 'lng'] }
    ]
  });
  sortFeatureList();

  map.on('moveend', function () {
    sortFeatureList();
  });
});
