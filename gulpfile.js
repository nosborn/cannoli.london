/* eslint-env node */

'use strict';

const gulp = require('gulp');

const awspublish = require('gulp-awspublish');
const babel = require('gulp-babel');
const cleanCss = require('gulp-clean-css');
const cloudfront = require('gulp-cloudfront');
const eslint = require('gulp-eslint');
const imagemin = require('gulp-imagemin');
const less = require('gulp-less');
const pug = require('gulp-pug');
const rename = require('gulp-rename');
const revAll = require('gulp-rev-all');
const sitemap = require('gulp-sitemap');
const uglify = require('gulp-uglify');
const yaml = require('gulp-yaml');

// BUILD TASKS

gulp.task('css', () => {
  gulp.src('src/css/*.less')
    .pipe(less()) // less().on('error', err => console.log(err))
    .pipe(cleanCss())
    .pipe(gulp.dest('build/css/'));
});

gulp.task('data', () => {
  gulp.src(['src/*.yaml', 'src/*.yml'])
    .pipe(yaml({schema: 'DEFAULT_SAFE_SCHEMA'}))
    .pipe(rename({extname: '.geojson'}))
    .pipe(gulp.dest('build/'));
});

gulp.task('html', () => {
  gulp.src('src/*.pug')
    .pipe(pug())
    .pipe(gulp.dest('build/'));
});

gulp.task('img', () => {
  gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('build/img/'));
});

gulp.task('js', () => {
  gulp.src('src/js/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(babel({presets: ['es2015']}))
    .pipe(uglify())
    .pipe(gulp.dest('build/js/'));
});

gulp.task('plain', () => {
  gulp.src('src/*.txt')
    .pipe(gulp.dest('build/'));
});

gulp.task('sitemap', () => {
  gulp.src('build/index.html', {
    read: false
  })
  .pipe(sitemap({
    siteUrl: 'https://osborn.io'
  }))
  .pipe(gulp.dest('build/'));
});

// DEPLOY TASKS

gulp.task('deploy', () => {
  const publisher = awspublish.create({
    region: process.env.AWS_DEFAULT_REGION,
    params: {
      Bucket: process.env.CONTENT_BUCKET
    }
  });

  const headers = {
    // 'Cache-Control': 'max-age=315360000, no-transform, public'
  };

  gulp.src('build/**')
    .pipe(revAll.revision({
      dontGlobal: [
        'ping.txt',
        'robots.txt',
        'sitemap.xml'
      ]
    }))
    .pipe(publisher.publish(headers))
    .pipe(awspublish.reporter())
    .pipe(cloudfront({
      bucket: process.env.CONTENT_BUCKET,
      distributionId: process.env.CLOUDFRONT_DISTRIBUTION_ID
    }))
    .pipe(publisher.sync());
});

// LOCAL TASKS

gulp.task('build', ['css', 'data', 'html', 'img', 'js', 'plain', 'sitemap']);
gulp.task('default', ['build']);
