module.exports = {
  "env": {
    "browser": true,
    "jquery": true
  },
  "extends": "eslint:recommended",
  "globals": {
    "Bloodhound": true,
    "Handlebars": true,
    "L": true,
    "List": true,
    "Raven": true,
    "UIkit": true,
    "opening_hours": true
  },
  "parserOptions": {
    "ecmaVersion": 6
  },
  "plugins": [
    "no-console-log"
  ],
  "root": true,
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "no-console": 0,
    "no-console-log/no-console-log": 1,
    "no-var": [
      "error"
    ],
    "quotes": [
      "error",
      "single"
    ],
    "semi": [
      "error",
      "always"
    ]
  }
};
