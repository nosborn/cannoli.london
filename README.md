# cannoli.london

[![build status](https://gitlab.com/nosborn/cannoli.london/badges/master/build.svg)](https://gitlab.com/nosborn/cannoli.london/commits/master)

## License

This project is licensed under the terms of the MIT license.
See the [LICENSE.txt](LICENSE.txt) file for details.
